<?php
/**
 * @file
 * test_view.features.inc
 */

/**
 * Implements hook_views_api().
 */
function test_view_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
